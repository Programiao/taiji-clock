
// ClockDlg.h : 头文件
//

#pragma once


// CClockDlg 对话框
class CClockDlg : public CDialogEx
{
private:
	enum bigSize{BAGUA = 550, TAIJI = 300, FU = 120, CLOCK = 420, 
				HBAGUA = 275, HTAIJI = 150, HFU = 60, HCLOCK = 210};
	NOTIFYICONDATA m_notify;
	CMenu m_menu;
	bool m_showBagua;
	int m_size;
	int m_bagua;
	int m_hbagua;
	int m_taiji;
	int m_htaiji;
	int m_fu;
	int m_hfu;
	int m_clock;
	int m_hclock;

	CDC * m_deskDc;
	CBitmap* m_deskBmp;

	Image m_taijiImg;
	bool m_lButtonDown;
	CPoint m_downPoint;
	CPoint m_downPos;


	void initPictures();
	void setSize(int size);
	void getDesktopImage();
	void readCfg();
	void writeCfg();
// 构造
public:
	CClockDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_CLOCK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg UINT OnNotifyFormat(CWnd *pWnd, UINT nCommand);
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()


public:
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSmall();
	afx_msg void OnMid();
	afx_msg void OnBig();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};
