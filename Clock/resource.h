//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Clock.rc
//
#define IDD_CLOCK_DIALOG                102
#define IDR_MENU1                       129
#define IDI_ICON1                       138
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_Refresh                      32776
#define ID_Small                        32777
#define ID_Mid                          32778
#define ID_Big                          32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
