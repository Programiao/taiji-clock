
// ClockDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Clock.h"
#include "ClockDlg.h"
#include "afxdialogex.h"
#include <math.h>
#include <fstream>
#include <iostream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CClockDlg 对话框



CClockDlg::CClockDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CClockDlg::IDD, pParent), m_taijiImg(_T("taiji.png"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

void CClockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CClockDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_NOTIFYFORMAT()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_Small, &CClockDlg::OnSmall)
	ON_COMMAND(ID_Mid, &CClockDlg::OnMid)
	ON_COMMAND(ID_Big, &CClockDlg::OnBig)
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

ULONG_PTR m_pGdiToken;
// CClockDlg 消息处理程序

BOOL CClockDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	
	//setSize(2);
	m_lButtonDown = false;
	/*int sWidth = GetSystemMetrics(SM_CXSCREEN); 
	int sHeight = GetSystemMetrics(SM_CYSCREEN); 
	int w = (sWidth - m_bagua) * 0.5;
	int h = (sHeight - m_bagua) * 0.5;*/
	m_showBagua = true;
	//MoveWindow(w, h, m_bagua, m_bagua);

	//SetWindowPos(&CWnd::wndBottom, w, h, m_bagua, m_bagua, SWP_NOOWNERZORDER);
	
	m_menu.LoadMenuW(IDR_MENU1);
	readCfg();
	//ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW);
/*

	CTime CurrentTime;
	CurrentTime=CTime::GetCurrentTime();
	CString time;
	time=CurrentTime.Format("%Y-%m-%d %H:%M:%S");

	m_notify.cbSize=sizeof(NOTIFYICONDATA);
	m_notify.dwInfoFlags=NIIF_INFO;
	m_notify.hIcon=m_hIcon;
	m_notify.hWnd=m_hWnd;
	wcscpy_s(m_notify.szTip,time);
	m_notify.uCallbackMessage=WM_NOTIFYFORMAT;
	m_notify.uFlags=NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_notify.uID=IDD_CLOCK_DIALOG;
	m_notify.uVersion=NOTIFYICON_VERSION;
	Shell_NotifyIcon(NIM_ADD,&m_notify);
*/

	CWnd* hDeskTop = FindWindow(_T("progman"), NULL);
	//hDeskTop = GetDesktopWindow();
	SetParent(hDeskTop);

	//init GDI+
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&m_pGdiToken, &gdiplusStartupInput, NULL); 

	initPictures();
	SetTimer(1,1000,NULL);

	m_deskDc = NULL;
	m_deskBmp = NULL;
	getDesktopImage();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CClockDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);

	}
	else
	{	
		CRect wndRect;
		GetWindowRect(&wndRect);
		int width = wndRect.right - wndRect.left;
		int height = wndRect.bottom - wndRect.top;

		CRect cltRect;
		GetClientRect(&cltRect);
		int cHeight = cltRect.bottom - cltRect.top;
		int capHeight = height - cHeight;

		int x = wndRect.left;
		int y = wndRect.top + capHeight;

		CPaintDC pdc(this);
		CDC cltMemDc; 
		CBitmap bitmap; 
		cltMemDc.CreateCompatibleDC( &pdc ); 
		bitmap.CreateCompatibleBitmap( &pdc, width, cHeight ); 
		cltMemDc.SelectObject( &bitmap ); 

		CWnd* hDeskTop = FindWindow(_T("Progman"), NULL);
		CDC* hDeskTopDc = hDeskTop->GetWindowDC();
		cltMemDc.BitBlt(0, 0, width, cHeight, m_deskDc, x, y, SRCCOPY);

		Graphics g (cltMemDc.m_hDC);
		//draw bagua
		if (m_showBagua)
		{
			Image bagua(_T("bagua.png"));
			g.DrawImage(&bagua, 0, 0, m_bagua, m_bagua);
		}
		//draw taiji 
		CTime currTime = CTime::GetCurrentTime();
		Image taiji(_T("taiji.png"));
		int second = currTime.GetSecond();
		g.TranslateTransform(m_hbagua, m_hbagua);
		GraphicsState tState = g.Save();
		g.RotateTransform(6 * second);
		g.DrawImage(&taiji, -m_htaiji, -m_htaiji, m_taiji, m_taiji);
		g.Restore(tState);

		//draw time fus
		Image black(_T("black.png"));
		Image white(_T("white.png"));
		int hour = currTime.GetHour();
		int minute = currTime.GetMinute();

		tState = g.Save();
		g.RotateTransform(minute * 6 + 0.1 * second);
		g.DrawImage(&white, -m_hfu, -m_hclock, m_fu, m_fu);
		g.Restore(tState);
		tState = g.Save();
		g.RotateTransform(hour * 30 + 0.5 * minute);
		g.DrawImage(&black, -m_hfu, -m_hclock, m_fu, m_fu);
		g.Restore(tState);
		//
		Graphics dtg(GetDesktopWindow()->m_hWnd);
		pdc.BitBlt( 0, 0, width, cHeight, &cltMemDc, 0, 0, SRCCOPY );
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CClockDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

UINT CClockDlg::OnNotifyFormat( CWnd *pWnd, UINT nCommand )
{
	/*if(nCommand==WM_RBUTTONDOWN || nCommand==WM_LBUTTONDOWN)
	{
		SetForegroundWindow();
		CMenu *pSubMenu;
		pSubMenu=m_menu.GetSubMenu(0);
		CPoint point;
		GetCursorPos(&point);
		pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_BOTTOMALIGN, point.x, point.y, this);
	}*/
	return CDialog::OnNotifyFormat(pWnd, nCommand);
}

void CClockDlg::OnTimer( UINT_PTR nIDEvent )
{
	if (nIDEvent == 1)
	{
		Invalidate(FALSE);
	}

	CDialog::OnTimer(nIDEvent);
}

void CClockDlg::initPictures()
{
	CDC* dc = GetDC();
	/*m_taijiImg.Load(_T("taiji.png"));
	HBITMAP hbitmap = m_taijiImg.Detach();
	CBitmap cBitmap;
	BITMAP bitmap;
	cBitmap.Attach(hbitmap);
	cBitmap.GetBitmap(&bitmap);
	m_taijiDc = new CDC;
	m_taijiDc->CreateCompatibleDC(dc);
	m_taijiDc->SelectObject(hbitmap);*/

/*
	m_taijiBmp.LoadBitmap(IDB_TAIJI);
	m_taijiDc = new CDC;
	m_taijiDc->CreateCompatibleDC(dc);
	m_taijiDc->SelectObject(m_taijiBmp);
	*/
}

LRESULT CClockDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(message==WM_NCPAINT)
	{
		//ShowWindow(SW_HIDE);
		return 0;
	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}

void CClockDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
	writeCfg();
	Gdiplus::GdiplusShutdown(m_pGdiToken);
}

void CClockDlg::setSize( int size )
{
	if (size <= 0)
	{
		return;
	}
	m_size = size;
	m_bagua = BAGUA / m_size;
	m_hbagua = m_bagua / 2;
	m_taiji = TAIJI / m_size;
	m_htaiji = m_taiji / 2;
	m_fu = FU / m_size;
	m_hfu = m_fu / 2;
	m_clock = CLOCK / m_size;
	m_hclock = m_clock / 2;

	CRect rect;
	GetWindowRect(&rect);
	MoveWindow(rect.left, rect.top, m_bagua, m_bagua);
}


void CClockDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	m_lButtonDown = true;
	CRect wndRect;
	GetWindowRect(&wndRect);
	m_downPoint = CPoint(point.x,  point.y);

	CDialogEx::OnLButtonDown(nFlags, point);
}


void CClockDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	m_lButtonDown = false;
	CDialogEx::OnLButtonUp(nFlags, point);
}


void CClockDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_lButtonDown)
	{	
		CRect rect;
		GetWindowRect(&rect);
		int x = point.x + rect.left - m_downPoint.x;
		int y = point.y + rect.top - m_downPoint.y;
		MoveWindow(x, y, m_bagua, m_bagua);
		//Invalidate(FALSE);

		//SetWindowPos(&CWnd::wndBottom, x, y, m_bagua, m_bagua, SWP_NOZORDER);
	}
	CDialogEx::OnMouseMove(nFlags, point);
}

void CClockDlg::getDesktopImage()
{
	int width = GetSystemMetrics(SM_CXSCREEN); 
	int height = GetSystemMetrics(SM_CYSCREEN); 

	CWnd* deskWnd = FindWindow(_T("progman"), NULL);
	CDC* deskDc = deskWnd->GetWindowDC();
	if (m_deskDc != NULL)
	{
		delete m_deskDc;
		m_deskDc = NULL;
	}
	m_deskDc = new CDC;
	m_deskDc->CreateCompatibleDC(deskDc);

	if (m_deskBmp != NULL)
	{
		delete m_deskBmp;
		m_deskBmp = NULL;
	}
	m_deskBmp = new CBitmap;
	m_deskBmp->CreateCompatibleBitmap(deskDc, width, height);
	m_deskDc->SelectObject(m_deskBmp);
	m_deskDc->BitBlt(0, 0, width, height, deskDc, 0, 0, SRCCOPY);
	
}

void CClockDlg::OnSmall()
{
	// TODO: 在此添加命令处理程序代码
	setSize(3);
	m_menu.GetSubMenu(0)->CheckMenuItem(0, MF_CHECKED | MF_BYPOSITION);
	m_menu.GetSubMenu(0)->CheckMenuItem(1, MF_UNCHECKED | MF_BYPOSITION);
	m_menu.GetSubMenu(0)->CheckMenuItem(2, MF_UNCHECKED | MF_BYPOSITION);
}


void CClockDlg::OnMid()
{
	// TODO: 在此添加命令处理程序代码
	setSize(2);
	m_menu.GetSubMenu(0)->CheckMenuItem(0, MF_UNCHECKED | MF_BYPOSITION);
	m_menu.GetSubMenu(0)->CheckMenuItem(1, MF_CHECKED | MF_BYPOSITION);
	m_menu.GetSubMenu(0)->CheckMenuItem(2, MF_UNCHECKED | MF_BYPOSITION);
}


void CClockDlg::OnBig()
{
	// TODO: 在此添加命令处理程序代码
	setSize(1);
	m_menu.GetSubMenu(0)->CheckMenuItem(0, MF_UNCHECKED | MF_BYPOSITION);
	m_menu.GetSubMenu(0)->CheckMenuItem(1, MF_UNCHECKED | MF_BYPOSITION);
	m_menu.GetSubMenu(0)->CheckMenuItem(2, MF_CHECKED | MF_BYPOSITION);
}


void CClockDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CMenu *pSubMenu;
	pSubMenu=m_menu.GetSubMenu(0);
	GetCursorPos(&point);
	pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_BOTTOMALIGN, point.x, point.y, this);

	CDialogEx::OnRButtonDown(nFlags, point);
}

void CClockDlg::readCfg()
{
	fstream f;
	f.open((_T("clk.cfg")), ios_base::in);
	int x, y;
	f>>x>>y>>m_size;
	setSize(m_size);
	MoveWindow(x, y, m_bagua, m_bagua);
	f.close();

	m_menu.GetSubMenu(0)->CheckMenuItem(3 - m_size, MF_CHECKED | MF_BYPOSITION);
}

void CClockDlg::writeCfg()
{
	fstream f;
	f.open((_T("clk.cfg")), ios_base::out);
	CRect r;
	GetWindowRect(&r);
	f<<r.left<<endl<<r.top<<endl<<m_size;
	f.close();
}
